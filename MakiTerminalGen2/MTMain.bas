'MakiTerminal Gen2
'(c) 2016-2018 MakiTelecom A.G.
'Version 2.0.0

'BEGIN Main code
PRINT "MakiTerminal 2.0.0"
PRINT "Please Wait...."

CALL INIT
CALL RestoreSCN
CLS
CALL GetVersion



SUB INIT
CLS
_FULLSCREEN
boot1& = _LOADIMAGE("MakiTerminal.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt0.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt1.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt2.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt3.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt4.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt5.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt6.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt7.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt8.png", 32)
SCREEN boot1&
SLEEP 1
boot1& = _LOADIMAGE("mt9.png", 32)
SCREEN boot1&
SLEEP 1


BEEP


END SUB

SUB RunPrompt
INPUT ">", cmd$
CALL GetCommand(cmd$)
END SUB

SUB GetCommand (cmd$)
c$ = LCASE$(cmd$)
IF c$ = "ver" THEN CALL GetVersion ELSE IF c$ = "birthday" THEN CALL Birthday ELSE IF c$ = "exit" THEN CALL EndProgram ELSE PRINT "ERROR: Command not found"
CALL RunPrompt

END SUB

SUB EndProgram
SYSTEM
END SUB


'END Main code

'BEGIN Graphics commands

SUB RestoreSCN
SCREEN 12
END SUB

'END Graphics

'Begin commands

SUB GetVersion
PRINT "MakiTerminal 2.0.0"
PRINT "Generation 2 Development Build"
PRINT "(c) 2016-2018 MakiTelecom A.G."
CALL RunPrompt
END SUB

SUB Birthday
PRINT "                                    ("
PRINT "                       ("
PRINT "               )                    )             ("
PRINT "                       )           (o)    )"
PRINT "               (      (o)    )     ,|,            )"
PRINT "              (o)     ,|,          |~\    (      (o)"
PRINT "              ,|,     |~\    (     \ |   (o)     ,|,"
PRINT "              \~|     \ |   (o)    |`\   ,|,     |~\"
PRINT "              |`\     |`\@@@,|,@@@@\ |@@@\~|     \ |"
PRINT "              \ | o@@@\ |@@@\~|@@@@|`\@@@|`\@@@o |`\"
PRINT "             o|`\@@@@@|`\@@@|`\@@@@\ |@@@\ |@@@@@\ |o"
PRINT "           o@@\ |@@@@@\ |@@@\ |@@@@@@@@@@|`\@@@@@|`\@@o"
PRINT "          @@@@|`\@@@@@@@@@@@|`\@@@@@@@@@@\ |@@@@@\ |@@@@"
PRINT "          p@@@@@@@@@@@@@@@@@\ |@@@@@@@@@@|`\@@@@@@@@@@@q"
PRINT "          @@o@@@@@@@@@@@@@@@|`\@@@@@@@@@@@@@@@@@@@@@@o@@"
PRINT "          @:@@@o@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@o@@::@"
PRINT "          ::@@::@@o@@@@@@@@@@@@@@@@@@@@@@@@@@@@o@@:@@::@"
PRINT "          ::@@::@@@@::oo@@@@oo@@@@@ooo@@@@@o:::@@@::::::"
PRINT "          %::::::@::::::@@@@:::@@@:::::@@@@:::::@@:::::%"
PRINT "          ::%%%::::::::::@::::::::::::::@::::::::::%%%::"
PRINT "        .#::%::%%%%%%:::::::::::::::::::::::::%%%%%::%::#."
PRINT "      .###::::::%%:::%:%%%%%%%%%%%%%%%%%%%%%:%:::%%:::::###."
PRINT "    .#####::::::%:::::%%::::::%%%%:::::%%::::%::::::::::#####."
PRINT "   .######`:::::::::::%:::::::%:::::::::%::::%:::::::::'######."
PRINT "   .#########``::::::::::::::::::::::::::::::::::::''#########."
PRINT "   `.#############```::::::::::::::::::::::::'''#############.'"
PRINT "    `.######################################################.'"
PRINT "      ` .################################################. '"
PRINT "         ` .##########################################. '"
PRINT "            `  .#################################.  '"
PRINT "                `  `  .###################.  '  '"
PRINT " "
PRINT "Happy Birthday Girlsies, Love Maki! :)"
CALL RunPrompt
END SUB

